# PHPBack Cloudron App

This repository contains the Cloudron app package source for [PHPBack](http://www.phpback.org).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.phpback.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.phpback.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd phpback-app
cloudron build
cloudron install
```
