FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

# get the source
RUN curl -L https://github.com/ivandiazwm/phpback/archive/v1.3.2.tar.gz | tar -xz --strip-components 1 -f -

# configure apache
RUN rm /etc/apache2/sites-enabled/*
ADD apache2-phpback.conf /etc/apache2/sites-enabled/phpback.conf
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN a2enmod rewrite

# configure mod_php
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 8M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 8M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

# skip initial installation wizard
RUN cp /app/code/install/database_tables.sql /app/code/database_tables.sql
RUN rm -rf /app/code/install/

# link application assets
RUN ln -s /run/database.php /app/code/application/config/database.php && \
    rm /app/code/application/config/config.php && ln -s /run/config.php /app/code/application/config/config.php

# patch to "consider" new ideas automatically
RUN sed -e "s/'status' => '.*',/'status' => 'considered',/" -i application/models/post.php

ADD config.php.template start.sh /app/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/start.sh" ]
