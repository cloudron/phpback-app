PHPBack is **not** integrated with the Cloudron user management.

The app comes with a pre-setup admin account with the following credentials:

`username`: admin@[Cloudron domain]

`password`: changeme

**Please change the admin password on first login**
