#!/bin/bash

set -eu

# create initial admin with:
#
# username: admin
# password: changme
#
# +----+-------+----------------------+--------------------------------------------------------------+-------+---------+--------+
# | id | name  | email                | pass                                                         | votes | isadmin | banned |
# +----+-------+----------------------+--------------------------------------------------------------+-------+---------+--------+
# |  1 | admin | admin@${MAIL_DOMAIN} | $2a$08$ALVyzlBEPHAazHaQPX/W..P8HwmCua41qn5kYFwAnJAIJWMq0XMb6 |    20 |       3 |      0 |
# +----+-------+----------------------+--------------------------------------------------------------+-------+---------+--------+
#
# created manually at the moment through the setup wizard

readonly mysql_cli="mysql -u${MYSQL_USERNAME} -p${MYSQL_PASSWORD} -h ${MYSQL_HOST} -P ${MYSQL_PORT} ${MYSQL_DATABASE}"

if ! $mysql_cli -e "select * from settings;"; then
    echo "=> Init database schema"
    cat /app/code/database_tables.sql | $mysql_cli

    echo "=> Create admin user"
    readonly PASSWORD="\$2a\$08\$ALVyzlBEPHAazHaQPX/W..P8HwmCua41qn5kYFwAnJAIJWMq0XMb6"
    $mysql_cli -e "INSERT INTO users (name, email, pass, votes, isadmin, banned) VALUES ('admin', 'admin@${MAIL_DOMAIN}', '${PASSWORD}', 20, 3, 0);"

    echo "=> Create basic settings"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('title', 'Cloudron Feedback');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('welcometext-title', 'Welcome to your user feedback app');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('welcometext-description', 'Here you can suggest ideas to improve our services or vote on ideas from other people');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('recaptchapublic', '');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('recaptchaprivate', '');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('language', 'english');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('maxvotes', '20');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('max_results', '10');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('mainmail', '${MAIL_FROM}');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('smtp-host', '${MAIL_SMTP_SERVER}');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('smtp-port', '${MAIL_SMTP_PORT}');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('smtp-user', '${MAIL_SMTP_USERNAME}');"
    $mysql_cli -e "INSERT INTO settings (name, value) VALUES ('smtp-pass', '${MAIL_SMTP_PASSWORD}');"

    echo "=> Create initial category"
    $mysql_cli -e "INSERT INTO categories (name, description, ideas) VALUES ('First Category', 'My first category', 0);"

    echo "=> Create initial idea"
    $mysql_cli -e "INSERT INTO ideas (title, content, authorid, date, votes, comments, status, categoryid) VALUES ('Hello there', 'This is an idea description', 1, NOW(), 0, 0, 'considered', 1);"
else
    # TODO update mail settings
    echo "=> Update SMTP settings"

    $mysql_cli -e "UPDATE settings SET value='${MAIL_FROM}' WHERE name='mainmail';"
    $mysql_cli -e "UPDATE settings SET value='${MAIL_SMTP_SERVER}' WHERE name='smtp-host';"
    $mysql_cli -e "UPDATE settings SET value='${MAIL_SMTP_PORT}' WHERE name='smtp-port';"
    $mysql_cli -e "UPDATE settings SET value='${MAIL_SMTP_USERNAME}' WHERE name='smtp-user';"
    $mysql_cli -e "UPDATE settings SET value='${MAIL_SMTP_PASSWORD}' WHERE name='smtp-pass';"
fi

echo "=> Update database settings"
cat > /run/database.php <<EOF
<?php \$active_group = 'default';
\$active_record = TRUE;
\$db['default']['hostname'] = '${MYSQL_HOST}';
\$db['default']['port'] = '${MYSQL_PORT}';
\$db['default']['username'] = '${MYSQL_USERNAME}';
\$db['default']['password'] = '${MYSQL_PASSWORD}';
\$db['default']['database'] = '${MYSQL_DATABASE}';
\$db['default']['dbdriver'] = 'mysqli';
\$db['default']['dbprefix'] = '';
\$db['default']['pconnect'] = TRUE;
\$db['default']['db_debug'] = TRUE;
\$db['default']['cache_on'] = FALSE;
\$db['default']['cachedir'] = '';
\$db['default']['char_set'] = 'utf8';
\$db['default']['dbcollat'] = 'utf8_general_ci';
\$db['default']['swap_pre'] = '';
\$db['default']['autoinit'] = TRUE;
\$db['default']['stricton'] = FALSE;
EOF

echo "=> Update app origin"
sed -e "s,##APP_ORIGIN##,${APP_ORIGIN}," /app/config.php.template > /run/config.php

echo "=> Ensure directories and permissions"
mkdir -p /run/sessions
chown -R www-data.www-data /run/

echo "=> Start apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
