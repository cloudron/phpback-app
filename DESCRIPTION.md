### Overview

This app packages PHPBack 1.3.2

PHPBack is feedback a web application that you can easily implement on your website.
It gives your customers a way to communicate their ideas to improve your products.

### Features

**Find Idea**

Your customers have new ideas that can help you to improve your products.
They will register through your implementation PHPBack (http://yoursite.com/feedback/ for example) and start sharing their ideas trough the system.

**Review Idea**

Before the idea get published, a moderator should change the idea to "Under Consideration" if it is found it can be considered.
So now the idea is published in your PHPBack portal, and other users/moderators can comment the idea, give their opinions and vote it!

**Implement Idea**

Once an in idea gets enough relevance, you can decide if you will implement the idea.
If you find out it cannot be done you can change the status to "Decined" at any time. The status flow of the ideas is:
Under Consideration -> Planned -> Started -> Completed
