#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    var TEST_TIMEOUT = 30000;
    var LOCATION = 'test';

    var app;
    var adminUser = 'admin';
    var adminEmail;
    var adminPassword = 'changeme';

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function login(callback) {
        browser.get('https://' + app.fqdn + '/home/login').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@name="email"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//input[@name="email"]'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="email"]')).sendKeys(adminEmail);
        }).then(function () {
            browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(adminPassword);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[text()="' + adminUser + '"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//*[text()="' + adminUser + '"]'))), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn + '/action/logout').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//button[text()="Log in"]')), TEST_TIMEOUT); // should see admin user
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//button[text()="Log in"]'))), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get fqdn', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        adminEmail = adminUser + '@' + inspect.fqdn;
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait --app ' + app.id);
        done();
    });

    it('can login', login);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id org.phpback.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can login', login);
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

});
